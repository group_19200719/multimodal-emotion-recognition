Данный репозиторий содержит код для обучения модели из научной статьи `Self-attention fusion for audiovisual emotion recognition with incomplete data`([ссылка](https://arxiv.org/abs/2201.11095)).

Цитирование научной статьи:

```
@article{chumachenko2022self,
  title={Self-attention fusion for audiovisual emotion recognition with incomplete data},
  author={Chumachenko, Kateryna and Iosifidis, Alexandros and Gabbouj, Moncef},
  journal={arXiv preprint arXiv:2201.11095},
  year={2022}
}
```

**Данный репозиторий использует код из оригинального репозитория от авторов научной статьи** ([ссылка](https://github.com/katerynaCh/multimodal-emotion-recognition)).

Единственное отличие этого репозитория заключается в поддержке `MlFlow` для логирования экспериментов.